// Copyright (c) 2019 Greg Griffith
// Copyright (c) 2019 The Bitcoin Unlimited developers
// Distributed under the MIT software license, see the accompanying
// file COPYING or http://www.opensource.org/licenses/mit-license.php.

#include "recursive_shared_mutex.h"
#include "test_threadsupport.h"

#include <atomic>

class rsm_watcher : public recursive_shared_mutex
{
public:
    size_t get_shared_owners_count() { return _read_owner_ids.size(); }
};

static rsm_watcher rsm;
static std::vector<int> rsm_guarded_vector;
static std::atomic<int> shared_locks {0};
static std::atomic<bool> proceed_with_promotion {false};

static void shared_only()
{
    rsm.lock_shared();
    shared_locks.store(shared_locks.load() + 1);
    while (proceed_with_promotion.load() == false) ;
    rsm.unlock_shared();
}

static void exclusive_only()
{
    rsm.lock();
    rsm_guarded_vector.push_back(4);
    rsm.unlock();
}

static void promoting_thread()
{
    rsm.lock_shared();
    shared_locks.store(shared_locks.load() + 1);
    bool promoted = rsm.try_promotion();
    assert(promoted == true);
    rsm_guarded_vector.push_back(7);
    rsm.unlock();
    rsm.unlock_shared();
}

/*
 * if a thread askes for a promotion while no other thread
 * is currently asking for a promotion it will be put in line to grab the next
 * exclusive lock even if another threads are waiting using lock()
 *
 * This test covers blocking of additional shared ownership aquisitions while
 * a thread is waiting for promotion.
 *
 */
void rsm_test_starvation()
{
    // clear the data vector at test start
    rsm_guarded_vector.clear();

    // start up intial shared thread to block immidiate exclusive grabbing
    std::thread one(shared_only);
    while(shared_locks.load() != 1) ;
    std::thread two(shared_only);
    while(shared_locks.load() != 2) ;
    std::thread three(promoting_thread);
    // wait for the first three threads to lock
    while(shared_locks.load() != 3) ;
    std::thread four(exclusive_only);
    // we should always get 3 because five, six, and seven should be blocked by
    // three promotion request leaving only one, two, and three with shared ownership
    assert(rsm.get_shared_owners_count() == 3);
    std::thread five(shared_only);
    assert(rsm.get_shared_owners_count() == 3);
    std::thread six(shared_only);
    assert(rsm.get_shared_owners_count() == 3);
    std::thread seven(shared_only);
    assert(rsm.get_shared_owners_count() == 3);
    // After testing that we could not lock shared while a promotion is waiting,
    // proceed with promotion
    proceed_with_promotion.store(true);

    one.join();
    two.join();
    three.join();
    four.join();
    five.join();
    six.join();
    seven.join();

    // 7 was added by the promoted thread, it should appear first in the vector
    rsm.lock_shared();
    assert(7 == rsm_guarded_vector[0]);
    assert(4 == rsm_guarded_vector[1]);
    rsm.unlock_shared();
}

void rsm_starvation_tests()
{
    rsm_test_starvation();
}
