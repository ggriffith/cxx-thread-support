// Copyright (c) 2021 Greg Griffith
// Distributed under the MIT software license, see the accompanying
// file COPYING or http://www.opensource.org/licenses/mit-license.php.

#include "promotable_shared_mutex.h"
#include "test_threadsupport.h"

static promotable_shared_mutex psm;

// basic lock and unlock tests
void psm_lock_unlock()
{
    // exclusive lock once
    psm.lock();

// try to unlock_shared an exclusive lock
// we should error here because exclusive locks can
// be not be unlocked by shared_ unlock method
#ifdef DEBUG_ASSERTION
    try
    {
        psm.unlock_shared();
        assert(false);
    }
    catch (const std::logic_error &e)
    {
        // intentionally do nothing
    }
#endif

    // unlock exclusive lock
    try
    {
        psm.unlock();
    }
    catch (const std::logic_error &e)
    {
        assert(false);
    }

    // exclusive lock once
    psm.lock();

    // try to unlock exclusive lock
    try
    {
        psm.unlock();
    }
    catch (const std::logic_error &e)
    {
        assert(false);
    }

#ifdef DEBUG_ASSERTION
    // try to unlock exclusive lock more times than we locked
    try
    {
        psm.unlock();
        assert(false);
    }
    catch (const std::logic_error &e)
    {
        // intentionally do nothing
    }
#endif

    // test complete
}

// basic lock_shared and unlock_shared tests
void psm_lock_shared_unlock_shared()
{
    // lock shared
    psm.lock_shared();

#ifdef DEBUG_ASSERTION
    // try to unlock exclusive when we only have shared
    try
    {
        psm.unlock();
        assert(false);
    }
    catch (const std::logic_error &e)
    {
        // intentionally do nothing
    }
#endif

    // unlock shared
    psm.unlock_shared();

#ifdef DEBUG_ASSERTION
    // we should error here because we are unlocking more times than we locked
    try
    {
        psm.unlock_shared();
        assert(false);
    }
    catch (const std::logic_error &e)
    {
        // intentionally do nothing
    }
#endif

    // test complete
}

// basic try_lock tests
void psm_try_lock()
{
    // try lock
    psm.try_lock();

#ifdef DEBUG_ASSERTION
    // try to unlock_shared an exclusive lock
    // we should error here because exclusive locks can
    // be not be unlocked by shared_ unlock method
    try
    {
        psm.unlock_shared();
        assert(false);
    }
    catch (const std::logic_error &e)
    {
        // intentionally do nothing
    }
#endif

    // unlock exclusive lock
    try
    {
        psm.unlock();
    }
    catch (const std::logic_error &e)
    {
        assert(false);
    }

    // try lock
    psm.try_lock();

    // try to unlock exclusive lock
    try
    {
        psm.unlock();
    }
    catch (const std::logic_error &e)
    {
        assert(false);
    }

#ifdef DEBUG_ASSERTION
    // try to unlock exclusive lock more times than we locked
    try
    {
        psm.unlock();
        assert(false);
    }
    catch (const std::logic_error &e)
    {
        // intentionally do nothing
    }
#endif

    // test complete
}

// basic try_lock_shared tests
void psm_try_lock_shared()
{
    // try lock shared
    psm.try_lock_shared();

#ifdef DEBUG_ASSERTION
    // unlock exclusive while we have shared lock
    try
    {
        psm.unlock();
        assert(false);
    }
    catch (const std::logic_error &e)
    {
        // intentionally do nothing
    }
#endif

    // unlock shared
    try
    {
        psm.unlock_shared();
    }
    catch (const std::logic_error &e)
    {
        assert(false);
    }

#ifdef DEBUG_ASSERTION
    // we should error here because we are unlocking more times than we locked
    try
    {
        psm.unlock_shared();
        assert(false);
    }
    catch (const std::logic_error &e)
    {
        // intentionally do nothing
    }
#endif

    // test complete
}

void psm_simple_tests()
{
    psm_lock_unlock();
    psm_lock_shared_unlock_shared();
    psm_try_lock();
    psm_try_lock_shared();
}
