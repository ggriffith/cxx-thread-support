// Copyright (c) 2019 Greg Griffith
// Copyright (c) 2019 The Bitcoin Unlimited developers
// Distributed under the MIT software license, see the accompanying
// file COPYING or http://www.opensource.org/licenses/mit-license.php.

#include "recursive_shared_mutex.h"
#include "test_threadsupport.h"

static recursive_shared_mutex rsm;

// basic lock and unlock tests
void rsm_lock_unlock()
{
    // exclusive lock once
    rsm.lock();

// try to unlock_shared an exclusive lock
// we should error here because exclusive locks can
// be not be unlocked by shared_ unlock method
#ifdef DEBUG_ASSERTION
    try
    {
        rsm.unlock_shared();
        assert(false);
    }
    catch (const std::logic_error &e)
    {
        // intentionally do nothing
    }
#endif

    // unlock exclusive lock
    try
    {
        rsm.unlock();
    }
    catch (const std::logic_error &e)
    {
        assert(false);
    }

    // exclusive lock once
    rsm.lock();

    // try to unlock exclusive lock
    try
    {
        rsm.unlock();
    }
    catch (const std::logic_error &e)
    {
        assert(false);
    }

#ifdef DEBUG_ASSERTION
    // try to unlock exclusive lock more times than we locked
    try
    {
        rsm.unlock();
        assert(false);
    }
    catch (const std::logic_error &e)
    {
        // intentionally do nothing
    }
#endif

    // test complete
}

// basic lock_shared and unlock_shared tests
void rsm_lock_shared_unlock_shared()
{
    // lock shared
    rsm.lock_shared();

#ifdef DEBUG_ASSERTION
    // try to unlock exclusive when we only have shared
    try
    {
        rsm.unlock();
        assert(false);
    }
    catch (const std::logic_error &e)
    {
        // intentionally do nothing
    }
#endif

    // unlock shared
    rsm.unlock_shared();

#ifdef DEBUG_ASSERTION
    // we should error here because we are unlocking more times than we locked
    try
    {
        rsm.unlock_shared();
        assert(false);
    }
    catch (const std::logic_error &e)
    {
        // intentionally do nothing
    }
#endif

    // test complete
}

// basic try_lock tests
void rsm_try_lock()
{
    // try lock
    rsm.try_lock();

#ifdef DEBUG_ASSERTION
    // try to unlock_shared an exclusive lock
    // we should error here because exclusive locks can
    // be not be unlocked by shared_ unlock method
    try
    {
        rsm.unlock_shared();
        assert(false);
    }
    catch (const std::logic_error &e)
    {
        // intentionally do nothing
    }
#endif

    // unlock exclusive lock
    try
    {
        rsm.unlock();
    }
    catch (const std::logic_error &e)
    {
        assert(false);
    }

    // try lock
    rsm.try_lock();

    // try to unlock exclusive lock
    try
    {
        rsm.unlock();
    }
    catch (const std::logic_error &e)
    {
        assert(false);
    }

#ifdef DEBUG_ASSERTION
    // try to unlock exclusive lock more times than we locked
    try
    {
        rsm.unlock();
        assert(false);
    }
    catch (const std::logic_error &e)
    {
        // intentionally do nothing
    }
#endif

    // test complete
}

// basic try_lock_shared tests
void rsm_try_lock_shared()
{
    // try lock shared
    rsm.try_lock_shared();

#ifdef DEBUG_ASSERTION
    // unlock exclusive while we have shared lock
    try
    {
        rsm.unlock();
        assert(false);
    }
    catch (const std::logic_error &e)
    {
        // intentionally do nothing
    }
#endif

    // unlock shared
    try
    {
        rsm.unlock_shared();
    }
    catch (const std::logic_error &e)
    {
        assert(false);
    }

#ifdef DEBUG_ASSERTION
    // we should error here because we are unlocking more times than we locked
    try
    {
        rsm.unlock_shared();
        assert(false);
    }
    catch (const std::logic_error &e)
    {
        // intentionally do nothing
    }
#endif

    // test complete
}

// test locking recursively 100 times for each lock type
void rsm_100_lock_test()
{
    uint8_t i = 0;
    // lock
    while (i < 100)
    {
        try
        {
            rsm.lock();
        }
        catch (const std::logic_error &e)
        {
            assert(false);
        }
        ++i;
    }

    while (i > 0)
    {
        try
        {
            rsm.unlock();
        }
        catch (const std::logic_error &e)
        {
            assert(false);
        }
        --i;
    }

    // lock_shared
    while (i < 100)
    {
        try
        {
            rsm.lock_shared();
        }
        catch (const std::logic_error &e)
        {
            assert(false);
        }
        ++i;
    }

    while (i > 0)
    {
        try
        {
            rsm.unlock_shared();
        }
        catch (const std::logic_error &e)
        {
            assert(false);
        }
        --i;
    }

    // try_lock
    while (i < 100)
    {
        try
        {
            rsm.try_lock();
        }
        catch (const std::logic_error &e)
        {
            assert(false);
        }
        ++i;
    }

    while (i > 0)
    {
        try
        {
            rsm.unlock();
        }
        catch (const std::logic_error &e)
        {
            assert(false);
        }
        --i;
    }

    // try_lock_shared
    while (i < 100)
    {
        try
        {
            rsm.try_lock_shared();
        }
        catch (const std::logic_error &e)
        {
            assert(false);
        }
        ++i;
    }

    while (i > 0)
    {
        try
        {
            rsm.unlock_shared();
        }
        catch (const std::logic_error &e)
        {
            assert(false);
        }
        --i;
    }

    // test complete
}

void rsm_simple_tests()
{
    rsm_lock_unlock();
    rsm_lock_shared_unlock_shared();
    rsm_try_lock();
    rsm_try_lock_shared();
    rsm_100_lock_test();
}
