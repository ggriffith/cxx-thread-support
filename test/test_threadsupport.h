// Copyright (c) 2021 Greg Griffith
// Distributed under the MIT software license, see the accompanying
// file COPYING or http://www.opensource.org/licenses/mit-license.php.

#ifndef THREADSUPPORT_TEST_TESTSETUP_H
#define THREADSUPPORT_TEST_TESTSETUP_H

#include <cassert>
#include <cstdio>
#include <stdexcept>
#include <vector>

void psm_promotion_tests();

void psm_simple_tests();

void psm_starvation_tests();

void rsm_promotion_tests();

void rsm_simple_tests();

void rsm_starvation_tests();

#endif // TEST_TESTSETUP_H
